// This file is subjected to the MIT License.
// Copyright (c) 2024 Adrien CL

// Global variables
let jsonArray;
let timerExecuted = false;
let timerPaused = false;
let timerReset = false;
let lastTime = 0;

// Utility function to wait for a given number of milliseconds
const wait = (milliseconds) => new Promise(resolve => setTimeout(resolve, milliseconds));

// Function to freeze execution until timerPaused becomes false
const freezeAsync = () => new Promise(resolve => {
  const checkCondition = () => {
    if (!timerPaused) {
      resolve();
    } else if (timerReset) {
	  throw new Error('Timer has been reset');
	} else {
      setTimeout(checkCondition, 100);
    }
  };
  checkCondition();
});

// ===== Carousel =====//

// Function to set the innerHTML of a carousel item safely
const setCarouselItem = (id, name) => {
  try {
    document.getElementById(id).innerHTML = name;
  } catch (error) {
    console.error(`Failed to set carousel item ${id}:`, error);
  }
};

// Function to initialize slide names in the carousel
const initiateSlideNames = () => {
  setCarouselItem("carouselitem4", "");
  setCarouselItem("carouselitem3", jsonArray.slides[0]?.name || "");
  setCarouselItem("carouselitem2", jsonArray.slides[1]?.name || "");
  setCarouselItem("carouselitem1", "");
};

// Function to change the slides in the carousel
const changeSlide = async (newItemText = "") => {
  // Declare carousel and children
  const carousel = document.getElementById("carousel");
  const carouselitem1 = document.getElementById("carouselitem1");
  const carouselitem2 = document.getElementById("carouselitem2");
  const carouselitem3 = document.getElementById("carouselitem3");
  const carouselitem4 = document.getElementById("carouselitem4");

  // Apply transition and change slide classes
  carousel.style.transition = "1s";
  if (document.body.clientWidth >= 1280) {
    carousel.style.left = "4px";
    carouselitem2.classList.add("mainitem");
    carouselitem3.classList.remove("mainitem");

    // Wait for transition to finish
    await wait(1000);
  }

  // Update slide content
  carousel.style.transition = "0s";
  carouselitem4.innerHTML = carouselitem3.innerHTML;
  carouselitem3.innerHTML = carouselitem2.innerHTML;
  carouselitem2.innerHTML = carouselitem1.innerHTML;
  carouselitem1.innerHTML = newItemText;

  if (document.body.clientWidth >= 1280) {
    // Reset carousel position and classes
    carousel.style.left = "-33%";
    carouselitem3.classList.add("mainitem");
    carouselitem2.classList.remove("mainitem");
  }
};

// ===== Import JSON file ===== //

// Function to open the file dialog for JSON file selection
const openFileDialog = () => document.getElementById('jsonfileinput').click();

// Function to handle file selection and JSON parsing
const onFileSelected = (event) => {
  const selectedFile = event.target.files[0];
  const reader = new FileReader();

  reader.onload = (event) => {
    try {
      jsonArray = JSON.parse(event.target.result);
      initiateSlideNames();
      timerPaused = false;
    } catch (error) {
      alert("An error occurred when importing JSON file. Please check the JS console.");
      console.error("Failed to parse JSON:", error);
    }
  };

  reader.readAsText(selectedFile);
};

// ===== Timer and slides ===== //

// Main timer function to handle countdown or count-up
const slideTimer = async (seconds, elementID, reverse = false, flash = false) => {
  if (!timerExecuted && !timerReset) {
    timerExecuted = true;
    let currentSecond = reverse ? lastTime || seconds : lastTime || 0;
    lastTime = 0;

    // Loop condition to handle reverse and non-reverse timing
    while ((reverse && currentSecond > 0) || (!reverse && currentSecond < seconds)) {
      if (timerPaused) {
        lastTime = currentSecond;
        return;
      }

      document.getElementById(elementID).innerHTML = currentSecond;
      await wait(1000);
      await freezeAsync();
	  if (timerReset) {throw new Error('Timer has been reset.') -1;}

      reverse ? currentSecond-- : currentSecond++;
    }

    document.getElementById(elementID).innerHTML = "Over";

    // Flash effect if specified
    if (flash) {
      const element = document.getElementById(elementID);
      const originalBg = element.style.backgroundColor;

      for (let i = 0; i < 10; i++) {
        element.style.backgroundColor = i % 2 === 0 ? originalBg : '#ff5733';
        await wait(100);
      }

      element.style.backgroundColor = originalBg;
    }
	
	timerExecuted = false;
  }
};

// Function to start the slide show and manage timing for each slide
const startSlides = async (jsonArray) => {
  for (let i = 0; i < jsonArray.slides.length; i++) {
    setCarouselItem("carouselitem4", jsonArray.slides[i - 1]?.name || "");
    setCarouselItem("carouselitem3", jsonArray.slides[i]?.name || "");
    setCarouselItem("carouselitem2", jsonArray.slides[i + 1]?.name || "");
    setCarouselItem("carouselitem1", jsonArray.slides[i + 2]?.name || "");

    try {await slideTimer(jsonArray.slides[i].time, "timer", true, true);} catch (error) {return;}

    if (i < jsonArray.slides.length - 1 && !timerReset) {
      await changeSlide(jsonArray.slides[i + 3]?.name || "");
    }
	try {await freezeAsync();} catch (error) {return;}
	if (timerReset) {initiateSlideNames;return;}
  }
};

// ===== Button actions ===== //

// Function to stop the timer
const stopTimer = () => {
  timerPaused = true;
};

// Function to start the timer
const startTimer = () => {
  if (timerPaused) {
    timerPaused = false;
  } else if (!timerExecuted && !timerPaused && !timerReset) {
    startSlides(jsonArray);
  };
};

// Function to reset the timer and initialize slides
const resetTimer = async () => {
  stopTimer();
  timerExecuted = false;
  timerPaused = false;
  timerReset = true;
  await wait(500);
  document.getElementById('timer').innerHTML = "Slide Timer";
  initiateSlideNames();
  await wait(1500);
  lastTime = 0;
  timerReset = false;
};

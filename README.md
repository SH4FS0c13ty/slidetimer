# SlideTimer

SlideTimer is a web timer for presentations. Based on a JSON file you submit, it will start a countdown for each slide and flash when the time is over, then it will process to the next slide countdown.

## Getting started

Simply download the source code and open the "index.html" file. Or host it on a webserver.
No external resources needed, everything is there.

## Demo
![SlideTimer GIF demonstration](https://s10.gifyu.com/images/SryVP.gif)

## JSON format
```
{
    "slides": [
        {"name": "<slide_name>", "time": <seconds>},
        {"name": "<slide_name>", "time": <seconds>},
        ...
    ]
}
```

## Contributing

Pull requests are welcome. For major changes or bug discovery, please [open an issue](https://gitlab.com/SH4FS0c13ty/slidetimer/issues) mentioning the name of the script in the title, to discuss changes.

## Credits

Thanks to @ajlkn who created the HTML template for [HTML5UP.net](https://html5up.net/) this project is using.


## License

The JavaScript file "assets/js/slidetimer.js" is subjected to the [MIT License](https://choosealicense.com/licenses/mit/).
All of the other files are subjected to the [Creative Commons Attribution 3.0 License](https://creativecommons.org/licenses/by/3.0/deed).
